// Compile with:
//  icc -std=c++11 -o fib_tbb fib_tbb.cpp -ltbb
#include <iostream>

#include <cstdio>

#include "tbb/task_group.h"
#include "tbb/task_scheduler_init.h"
#include "tbb/tick_count.h"

int N = 30;

int Fib(int n) {
  if (n < 2) return n; 

  int x, y;

  tbb::task_group g;
  g.run([&] {x = Fib(n-1);} );
  g.run([&] {y = Fib(n-2);} );
  g.wait();  

  return x + y;
}

int main() {

  int n = tbb::task_scheduler_init::default_num_threads();
  for (int p = 1; p <= n + 4; ++p) {
    // Construct task scheduler with p threads
    tbb::task_scheduler_init init(p);
    tbb::tick_count t0 = tbb::tick_count::now();

    int rv = Fib(N);
    //printf("Fib of %d = %d\n", N, rv);


    tbb::tick_count t1 = tbb::tick_count::now();
    double t = (t1 - t0).seconds();
    std::cout << "time = " << t * 1000 << "(ms) with " << p << " threads" << std::endl;
    // Implicitly destroy task scheduler
  }


}


