#include <iostream>

#include <cstdio>

int N = 30;

int Fib(int n) {
  if (n < 2) return n; 

  int x = Fib(n-1);
  int y = Fib(n-2);
  return x + y;
}

int main() {

  int rv = Fib(N);
  printf("Fib of %d = %d\n", N, rv);

}


