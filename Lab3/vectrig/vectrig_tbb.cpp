// Compile using:
//  icc -o vectrig_tbb vectrig_tbb.cpp -ltbb
#include <iostream>
#include <vector>

#include <cstdio>
#include <cmath>

#include "tbb/parallel_for.h"
#include "tbb/blocked_range.h"
#include "tbb/task_scheduler_init.h"
#include "tbb/tick_count.h"

int N = 10000;

class VecTrig {
  std::vector<double> &A, &B, &C;
  int N;

  public: 
  void operator() (const tbb::blocked_range<std::size_t> &r) const {
    if (r.begin() == 0) printf("begin: %d, end: %d\n", r.begin(), r.end());
    for (std::size_t i = r.begin(); i != r.end(); ++i) {
      C[i] = atan(A[i]) /  cos(B[i]);
    }
  }

  VecTrig(std::vector<double> &a, std::vector<double> &b, std::vector<double> &c, int n) :
    A(a), B(b), C(c), N(n) {} 
};

int main() {

  std::vector<double> A;
  std::vector<double> B;
  std::vector<double> C;

  A.resize(N); B.resize(N); C.resize(N);

  // initalize arrays
  for (int i = 0; i < N; ++i) {
    A[i] = i;
    B[i] = 2*i;
  }

  // vector multiplication
  int n = tbb::task_scheduler_init::default_num_threads();
  for (int p = 1; p <= n + 4; ++p) {
    tbb::task_scheduler_init init(p);
    tbb::tick_count t0 = tbb::tick_count::now();

    tbb::parallel_for(tbb::blocked_range<std::size_t>(0, N), VecTrig(A, B, C, N) );  

    tbb::tick_count t1 = tbb::tick_count::now();
    double t = (t1 - t0).seconds();
    std::cout << "time = " << t * 1000.0 << "(ms) with " << p << " threads" << std::endl;
  }

  // verification
  for (int i = 0; i < N; ++i) {
    if (C[i] - (atan(i) / cos(i*2)) > 1e-8 ) {
      printf("Verfication Failed C[%d] %f != %f\n", i, C[i], atan(i) / cos(i*2));
      return 0; 
    }
  }

  printf("Verification Successful!\n");
}
