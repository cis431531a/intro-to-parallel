#include <chrono>
#include <iostream>

#include <stdio.h>
#include <stdlib.h>
#include <omp.h>

typedef std::chrono::high_resolution_clock Clock;

int main (int argc, char *argv[]) 
{
  int   i, n;
  float a[1000000], b[1000000], sum; 

  /* Some initializations */
  n = 1000000;
  for (i=0; i < n; i++)
    a[i] = b[i] = i * 1.0;
  sum = 0.0;

  auto t1 = Clock::now();

  #pragma omp parallel shared(a, b, n) private(i)
  {
    #pragma omp for reduction(+:sum)
    for (i=0; i < n; i++)
      sum = sum + (a[i] * b[i]);
  }

  auto t2 = Clock::now();

  printf("   Sum = %f\n",sum);
  std::cout << "Runtime: "
    << std::chrono::duration_cast<std::chrono::nanoseconds>(t2 - t1).count()
    << " nanoseconds" << std::endl;
}
